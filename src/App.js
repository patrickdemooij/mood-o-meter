import GaugeChart from 'react-gauge-chart';
import './App.css';


function App() {
  return (
    <div className="App">
      <main>
        <h1>Mood-o-meter</h1>
        <h2>Risk analyser</h2>

        <GaugeChart id="gauge-chart"
          // hideText={true}
          nrOfLevels={20}
          needleBaseColor="white"
          needleColor="white"
          percent={round(calculateRisk())}
        />

        <table>
          <tr><td>sleep: </td><td>{round(getSleepRisk()) * 100}%</td></tr>
          <tr><td>sugar crash: </td><td>{round(getSugarCrashRisk()) * 100}%</td></tr>
          <tr><td>period: </td><td>{round(getPeriodRisk()) * 100}%</td></tr>
          <tr><td>PMS: </td><td>{round(getPmsRisk()) * 100}%</td></tr>
        </table>
      </main>
    </div>
  );
}

export default App;

// function clip(value) {
//   return Math.max(0, Math.min(1, value));
// }

function round(value) {
  return Math.round(value * 100) / 100
}

function calculateRisk() {
  const sugarCrash = getSugarCrashRisk();
  const sleep = getSleepRisk();
  const period = getPeriodRisk();
  const pms = getPmsRisk();
  
  return  sugarCrash * .2 +
          sleep * .4 +
          period * .4 +
          pms * .6;
}

function getSleepRisk() {
  const time = new Date();

  if (time.getHours() >= 22) {
    return 1;
  }

  if (time.getHours() >= 21) {
    return time.getMinutes() * 1 / 60;
  }

  return 0;
}

function getSugarCrashRisk() {
  if (laterThen(13, 30) &&
      earlierThen(15)) {
    const start = timestamp(13, 30).getTime();
    const minutesSinceStart = toMinutes(new Date().getTime() - start);

    return minutesSinceStart / 1/90;
  }

  return 0;
}

function getPeriodRisk() {
  const firstPeriodDay = new Date(2022, 3, 15);
  const days = ((new Date().getTime() - firstPeriodDay.getTime()) / 1000 / 60 / 60 / 24) % 28;
  
  if (days <= 6)
  {
    return 1 - (days / 6);
  }

  return 0;
}

function getPmsRisk() {
  const firstPeriodDay = new Date(2022, 3, 15);
  const days = ((new Date().getTime() - firstPeriodDay.getTime()) / 1000 / 60 / 60 / 24) % 28;
  
  if (days >= 23)
  {
    return (days - 23) / 5 * .7 + .3;
  }

  return 0;
}

function laterThen(hours, minutes = 0) {
  var date = new Date();
  return (date.getHours() === hours &&
          date.getMinutes() >= minutes) ||
          date.getHours() > hours;
}

function earlierThen(hours, minutes = 0) {
  var date = new Date();
  return (date.getHours() === hours &&
          date.getMinutes() <= minutes) ||
          date.getHours() < hours;
}

// function getCurrentTime() {
//   return new Date(new Date() - new Date(
//     new Date().getFullYear(),
//     new Date().getMonth(),
//     new Date().getDate(),
//     0,
//     0, 
//     0
//   ));
// }

function timestamp(hours, minutes) {
  return new Date(
    new Date().getFullYear(),
    new Date().getMonth(),
    new Date().getDate(),
    hours,
    minutes,
    0
  );
}

// function getCurrentTime(hours, minutes) {
//   return new Date(Date.now().getTime() / 24 / 60 / 1000);
// }

function toMinutes(epoch) { 
  return epoch / 1000 / 60;
}